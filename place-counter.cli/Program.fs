﻿namespace PlaceCounter.Cli

open Spectre.Console.Cli

module Program =

    [<EntryPoint>]
    let main argv =

        let app = CommandApp()
        app.Configure(fun config ->
            config.AddCommand<GoogleLocation>("google")
                .WithAlias("g")
                .WithDescription("Searches your Google location history file to determine how many days you were at that location")
                |> ignore)

        app.Run(argv)
