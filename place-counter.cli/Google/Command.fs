namespace PlaceCounter.Cli

open System.ComponentModel
open System.IO
open GeoJSON.Net.Geometry
open Spectre.Console
open Spectre.Console.Cli
open SpectreCoff
open PlaceCounter.Google
open PlaceCounter

type GoogleLocationSettings() as self =
    inherit CommandSettings()

    [<Description("Latitude of the place to search")>]
    [<CommandOption("-a|--latitude")>]
    member val latitude = 0.0 with get, set

    [<Description("Longitude of the place to search")>]
    [<CommandOption("-o|--longitude")>]
    member val longitude = 0.0 with get, set

    [<Description("Radius from the POI in meters. Default: 100m")>]
    [<CommandOption("-r|--radius")>]
    member val radius = 100.0 with get, set

    [<Description("Print more information about the points found")>]
    [<CommandOption("-v|--verbose")>]
    member val verbose = false with get, set

    [<Description("Path to the Google Location 'Records.json' file")>]
    [<CommandArgument(0, "[filePath]")>]
    member val filePath = "" with get, set

    override _.Validate() =
        match self with
        | x when x.latitude > 90.0 || x.latitude < -90.0 ->
            ValidationResult.Error("Latitude must be between -90 and +90")
        | x when x.longitude > 180.0 || x.latitude < -180.0 ->
            ValidationResult.Error("Longitude must be between -180 and +180")
        | x when x.radius <= 0.0 -> ValidationResult.Error("Radius must be greater than 0")
        | x when not (File.Exists x.filePath) -> ValidationResult.Error("The given file path could not be found")
        | _ -> ValidationResult.Success()

type GoogleLocation() =
    inherit Command<GoogleLocationSettings>()

    let displayFullTable (results: (int * DistanceCalculator.TimePosition list) list) =
        results
        |> List.iter (fun (year, positions) ->
            let table =
                Table().AddColumn("[green][bold]Date[/][/]")
                   .AddColumn("[blue][bold]Latitude[/][/]")
                   .AddColumn("[blue][bold]Longitude[/][/]")

            table.Caption <- TableTitle(string year)

            positions
            |> List.iter (fun x ->
                table.AddRow(x.timestamp.ToString("f"), string x.Latitude, string x.Longitude)
                |> ignore)

            table.toOutputPayload |> toConsole)

        ()

    let displayResults (results: (int * DistanceCalculator.TimePosition list) list) verbose =
        let table =
            Table().AddColumn("[green][bold]Year[/][/]")
                .AddColumn("[blue][bold]# of Visits[/][/]")

        results
        |> List.iter (fun (year, positions) -> table.AddRow(string year, string positions.Length) |> ignore)

        table.toOutputPayload |> toConsole

        match verbose with
        | true -> displayFullTable results
        | false -> ()

        0

    let processInput (settings: GoogleLocationSettings) (status: StatusContext) =
        task {
            status.Status <- "Parsing JSON"

            use fileContents = File.OpenRead(settings.filePath)
            let! records = Json.deserialize<LocationRecords> fileContents
            C "JSON Parsed"  |> toConsole

            status.Status <- "Measuring distances..."

            let positions =
                records
                |> Result.map (fun x -> x.locations |> List.map (fun y -> y.toPosition ()))
                |> Result.map (
                    DistanceCalculator.countDaysNearPlace
                        (Position(settings.latitude, settings.longitude))
                        settings.radius
                )

            C "Distances measured"  |> toConsole

            return positions
        }

    interface ICommandLimiter<GoogleLocationSettings>

    override _.Execute(_context, settings) =
        BI
            [ (C $"Latitude: {settings.latitude}")
              (C $"Longitude: {settings.longitude}")
              (C $"Radius: {settings.radius} meters") ]
        |> toConsole

        let positions =
            AnsiConsole
                .Status()
                .StartAsync("[green]Processing your request...[/]", processInput settings)
            |> Async.AwaitTask
            |> Async.RunSynchronously

        match positions with
        | Ok x -> displayResults x settings.verbose
        | Error x ->
            E $"Error occurred: {x}" |> toConsole
            1
