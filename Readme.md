# Place Counter

Use your Google Location History to track how many days you visited a location each year.

## Installation

```sh
dotnet tool install --global place-counter.cli
```

## Usage

### Google Location Data

1. Download your Google Location Data from [Google Takeout](https://takeout.google.com/)
1. Extract the `Records.json` file from the downloaded Zip file
1. Run the following, replacing the sample path with the path to your downloaded `Records.json` file
    ```sh
    place-counter google "/Users/example/Downloads/Takeout/Location History/Records.json" -a 51.509865 -o -0.118092 -r 100
    ```

### Options

```
-h, --help         Prints help information                      
-a, --latitude     Latitude of the place to search              
-o, --longitude    Longitude of the place to search             
-r, --radius       Radius from the POI in meters. Default: 100m 
-v, --verbose      Print more information about the points found
```

## References

> [Calculate distance, bearing and more between Latitude/Longitude points](http://www.movable-type.co.uk/scripts/latlong.html)

> [Spectre.Console](https://spectreconsole.net/)

> [SpectreCoff](https://github.com/EluciusFTW/SpectreCoff)
