export interface FilesLoaded {
  files: FileList;
}

interface Location {
  latitudeE7: number;
  longitudeE7: number;
  timestamp: string;
}

interface GoogleLocationHistory {
  locations: Location[];
}

onmessage = (message: MessageEvent<FilesLoaded>) => {
  console.log("got message", message);
  const { files } = message.data;

  for (let i = 0; i < files.length; i++) {
    const file = files[i];

    file.text().then((jsonString) => {
      //console.time("JSON.parse");
      const records = JSON.parse(jsonString) as GoogleLocationHistory;

      //console.timeEnd("JSON.parse");
      console.log("First record:", records.locations[0])
    });
  }
};

export default {};
