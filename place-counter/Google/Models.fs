namespace PlaceCounter.Google

open System
open PlaceCounter.DistanceCalculator


type Location =
    {
        latitudeE7: double
        longitudeE7: double
        timestamp: DateTimeOffset
    }
    member this.toPosition (): TimePosition =
        TimePosition(this.latitudeE7 / 1e7, this.longitudeE7 / 1e7, this.timestamp)

type LocationRecords =
    {
        locations: Location list
    }
