module PlaceCounter.Json

open System.IO
open System.Text.Json
open System.Text.Json.Serialization

let private options = JsonFSharpOptions.Default().ToJsonSerializerOptions()

type JsonError =
    | ArgumentNull
    | JsonException of string
    | NotSupported of string

let deserialize<'T> (value: Stream) =
    task {
        try
            let! deserialized = JsonSerializer.DeserializeAsync<'T>(value, options)
            return Ok deserialized
        with ex -> return Error ex
    }
