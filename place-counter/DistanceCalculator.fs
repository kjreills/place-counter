module PlaceCounter.DistanceCalculator

open System
open GeoJSON.Net.Geometry


type TimePosition(lat: double, long: double, ts) =
    inherit Position(lat, long)
    member val timestamp: DateTimeOffset = ts

/// <summary>
/// Uses the Haversine formula to determine the straight-line
/// distance between 2 points on the surface of the earth
/// </summary>
/// <remarks>
/// Translated from the JavaScript code in this article
/// http://www.movable-type.co.uk/scripts/latlong.html
/// </remarks>
let getDistance (position1: Position) (position2: Position) =
    let R = 6371e3 // metres
    let toRadians x = x * Math.PI / 180.0
    
    let φ1 = position1.Latitude |> toRadians // φ, λ in radians
    let φ2 = position2.Latitude |> toRadians
    let Δφ = (position2.Latitude - position1.Latitude) |> toRadians
    let Δλ = (position2.Longitude - position1.Longitude) |> toRadians

    let a =
        (sin (Δφ / 2.0)) * sin (Δφ / 2.0)
        + cos φ1 * cos φ2 * sin (Δλ / 2.0) * sin (Δλ / 2.0)

    let c = 2.0 * (atan2 (sqrt a) (sqrt (1.0 - a)))

    let d = R * c // in metres
    d
    
let withinRadius (pointOfInterest: Position) (radius: float) (position: Position) =
    getDistance pointOfInterest position < radius

let countDaysNearPlace (pointOfInterest: Position) (radius: float) (timePositions: TimePosition list) =
    let closePoints =
        timePositions
        |> List.filter (withinRadius pointOfInterest radius)
        
    let grouped = closePoints
                  |> List.distinctBy (fun x -> x.timestamp.Date)
                  |> List.groupBy (fun x -> x.timestamp.Date.Year)
    grouped
